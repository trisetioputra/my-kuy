
$(document).ready(function() {
    $("#submit").on("click", function(z) {
        var x = z.currentTarget.previousElementSibling.value.toLowerCase()
        $.ajax({
            url: books_data_url + x,
            success: function(books) {
                $('#isi').html('')
                var result = '<tr>';

                for (var i = 0; i < books.items.length; i++) {
                    result += "<tr> " +
                        "<td>" + books.items[i].volumeInfo.title + "</td>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        books.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td>" + books.items[i].volumeInfo.authors + "</td>" +
                        "<td> <button id='like' class='button1' onclick='addLike()' name='likebut'> Like the book </button> </td>"+
                        "<td>" + books.items[i].volumeInfo.likes + "</td>" +
                        "<td>" + books.items[i].id + "</td>" 

                }
                $('#isi').append(result);
            },
        })
    });
});

$(document).ready(function() {
    $("#modbut").on("click", function(z) {
        $.ajax({
            url: top_book_url,
            success: function(books) {
                $('#klasemen').html('')
                var result = '';
                for (var i = 0; i < books.top5.length; i++) {
                    console.log(books.top5[i].cover)
                    result+= "No:" + parseInt(i+1) +"<br>"+ "Title: "+books.top5[i].title +"<br>"+
                    "Author: "  +books.top5[i].author + "<br>" +
                    "Cover: "+ books.top5[i].cover + "<br>"+
                    "Numbers of Likes: " + books.top5[i].likes + "<br>"

                }
                $('#klasemen').append(result);
            },
        })
    });
});

function addLike(){
      var z= parseInt(event.currentTarget.parentNode.nextElementSibling.innerHTML) +1
      event.currentTarget.parentNode.nextElementSibling.innerHTML= z
      var x= event.currentTarget.parentNode.nextElementSibling.innerHTML
      var ID=event.currentTarget.parentNode.nextElementSibling.nextElementSibling.innerHTML
      var aut=event.currentTarget.parentNode.previousElementSibling.innerHTML
      var cov=event.currentTarget.parentNode.previousElementSibling.previousElementSibling.innerHTML
      var tit=event.currentTarget.parentNode.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML
      const book={
          id:ID,
          cover:cov,
          title:tit,
          author:aut,
          likes:x
      }
      console.log(book)
      $.ajax({
          type:"POST",
          url:add_like_url,
          data:book

      })
}
