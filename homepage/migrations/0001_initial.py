# Generated by Django 3.0.3 on 2020-04-30 15:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.CharField(max_length=130, primary_key=True, serialize=False)),
                ('cover', models.CharField(max_length=400)),
                ('title', models.CharField(max_length=200)),
                ('author', models.CharField(blank=True, max_length=200)),
                ('likes', models.IntegerField(default=0)),
            ],
        ),
    ]
