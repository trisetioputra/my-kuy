from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase,Client
from django.urls import resolve
import time
from .views import Landing


class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)
        # find the form element
        searchbox = selenium.find_element_by_name('test1')

        submitbut = selenium.find_element_by_name('test4')
        searchbox.send_keys('unbroken')
        submitbut.send_keys(Keys.ENTER)


        time.sleep(5)

        page=selenium.find_element_by_tag_name('body').text

        self.assertIn('Unbroken',page)

        likebut= selenium.find_element_by_name('likebut')

        likebut.send_keys(Keys.RETURN)
        time.sleep(3)
        # Fill the form with data
        
        modal=selenium.find_element_by_name('mod')

        modal.send_keys(Keys.RETURN)
        time.sleep(3)

        


class unitTest(TestCase):
    def test_app_url_exist(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)
    def test_view_def_exist(self):
        found=resolve('/')
        self.assertEqual(found.func, Landing)
   
   
   





