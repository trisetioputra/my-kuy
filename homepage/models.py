
from django.db import models


class Book(models.Model):
    id = models.CharField(max_length=130, primary_key=True)
    cover = models.CharField(max_length=400)
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200, blank=True)
    likes = models.IntegerField(default=0)


   
 